module Tezos.Base.Unsafe (module X) where

import Tezos.Base.ProtocolConstants as X (unsafeEstimatePastTimestamp)
